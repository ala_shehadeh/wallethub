## Facebook assignment

To implement the app the following software as to be installed:
* PHP 7
* Composer
* Git

to execute the app please do the following:

* clone the repository by run this command on the cmd
git clone https://ala_shehadeh@bitbucket.org/ala_shehadeh/wallethub.git
* run the following command to install the dependencies and generate autoload files
/cloned path/wallethub/facebook> composer update
* run the following command to open the testing server:
/cloned path/wallethub/facebook>php -S localhost:port
* open /classes/faceBook.php and the appid, secert and return server url