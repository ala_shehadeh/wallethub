<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 9/7/2018
 * Time: 12:43 AM
 */

namespace walletHub;
use \Facebook\Facebook;
use Facebook\Exceptions;


class FB
{
    const APPID = 'insert your app id here';
    const SECRET = 'insert your app secret';
    const URL = 'insert your return url here';
    private $fb;

    public function __construct()
    {
        $this->fb = new Facebook([
            'app_id' => self::APPID,
            'app_secret' => self::SECRET,
            'default_graph_version' => 'v2.10',
        ]);
    }
    public function generateFacebookLink() {
        $helper = $this->fb->getRedirectLoginHelper();
        $loginUrl = $helper->getLoginUrl(self::URL.'/callback.php');
        return $loginUrl;
    }
    public function getAccessToken() {
        $helper = $this->fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        }
        catch(Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return $e->getMessage();
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                $ouput = array();
                $ouput['Error'] = $helper->getError();
                $ouput['Error Code'] = $helper->getErrorCode();
                $ouput['Error Reason'] = $helper->getErrorReason();
                $ouput['Error Description'] = $helper->getErrorDescription();
                return $ouput;
            } else
                return ['Bad request'];
        }
        else
            return $accessToken->getValue();
    }
    public function myData($accessToken) {
        try {
            $response = $this->fb->get('/me?fields=name,birthday,location', $accessToken);
        } catch(Exceptions\FacebookResponseException $e) {
            return $e->getMessage();
        } catch(Exceptions\FacebookSDKException $e) {
             return $e->getMessage();
        }

        $me = $response->getGraphUser();
        return $me;
    }
}