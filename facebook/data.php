<?php
session_start();
// Include the required dependencies.
require_once( 'vendor/autoload.php' );
use walletHub\FB;
$fb = new FB();

if(!$_SESSION['fb_access_token'])
    header("Location: /link.php");
else {
    $accessToken = $_SESSION['fb_access_token'];
    $data = $fb->myData($accessToken);
    echo 'I am '.$data->getName();
}