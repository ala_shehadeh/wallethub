<?php
session_start();

require_once( 'vendor/autoload.php' );
use walletHub\FB;

$fb = new FB();
$accessToken = $fb->getAccessToken();
if(is_array($accessToken)) {
    header('HTTP/1.0 401 Unauthorized');
    foreach ($accessToken as $value)
        echo '<li>'.$value.'</li>';
}
else {
    $_SESSION['fb_access_token'] = (string)$accessToken;
    header("Location: /data.php");
}