## Wallethub resize image assignment

To implement the app the following software as to be installed:
* PHP 7
* Composer
* Git

to execute the app please do the following:

* clone the repository by run this command on the cmd
git clone https://ala_shehadeh@bitbucket.org/ala_shehadeh/wallethub.git
* run the following command to install the dependencies and generate autoload files
/cloned path/wallethub/imageResize> composer update
* run the following command to resize the images
/cloned path/wallethub/imageResize> php resize.php [image name and page] [width] [height]
the width and height has to be integer value bigger than 0 and image path should be string pointed to exist file
