<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 9/5/2018
 * Time: 8:21 PM
 */
namespace walletHub;
class setVariablesClass
{
    function variablesValidation($variables) {
        $errors = array();
        if($variables[1] == '')
            $errors[] = 'You have to set the path of the image';
        if(!is_file($variables[1]))
            $errors[] = 'the selected image not exist at the server';
        if(intval($variables[2]) <= 0)
            $errors[] = 'The width has to be exist and larger than 0';
        if(intval($variables[3]) <= 0)
            $errors[] = 'The height has to be exist and larger than 0';
        return $errors;
    }
    function setVariables() {
        $variables = $_SERVER['argv'];
        $errors = $this->variablesValidation($variables);
        if(count($errors) > 0)
            return array('error'=>1,'data'=>$errors);
        else {
            $output = array();
            $output['file'] = $variables[1];
            $output['width'] = $variables[2];
            $output['height'] = $variables[3];
            return array('error' => 0, 'data' => $output);
        }
    }
}