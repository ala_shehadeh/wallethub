<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 9/5/2018
 * Time: 8:26 PM
 */
namespace walletHub;
use walletHub\resizeClass;
use walletHub\setVariablesClass;
use Exception;

final class operationClass
{
    private $resizeClass;
    private $setVariablesClass;

    public function __construct()
    {
        $this->setVariablesClass = new setVariablesClass();
    }
    public function output() {
        $variables = $this->setVariablesClass->setVariables();

        if($variables['error'] == 1)
            throw new Exception(print_r(implode(' , ', $variables['data'])));
        else {
            $this->resizeClass = new resizeClass($variables['data']['file'],$variables['data']['width'],$variables['data']['height']);
            $this->resizeClass->resizeImage();
            print 'The image resized correctly';
        }
    }
}