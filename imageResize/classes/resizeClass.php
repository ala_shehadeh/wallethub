<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 9/5/2018
 * Time: 7:33 PM
 */
namespace walletHub;
use Gumlet\ImageResize;
class resizeClass
{
    private $file;
    private $width;
    private $height;
    public function __construct($file,$width,$height)
    {
        $this->file = $file;
        $this->width = $width;
        $this->height = $height;
    }
    function resizeImage() {
        $image = new ImageResize($this->file);
        $image->resizeToBestFit($this->width, $this->height);
        $image->save($this->file);
    }
}